const newsList = document.getElementById('berita');
newsList.innerHTML = message('<div class="spinner-border text-danger mx-auto mt-5" role="status"><span class="visually-hidden">Loading...</span></div>');
const berita = fetch('https://newsapi.org/v2/top-headlines?country=id&apiKey=fec030d5fa9042b4a90c940be727e9a0');
berita
  .then(function(res){ return res.json()})
  .then(res => {
    newsList.innerHTML = render(res);
  })
  .catch(err => {
    // console.log("Some error occured")
      newsList.innerHTML = message(err.message)
  })
  .finally(() => {
      
  });

    function render(result) {
        let articles = result.articles;
        console.log(articles);
        let newsHTML = '';
        articles.forEach(function(element) {
            let news = `<div class="col-md-4 mt-5">
                        <div class="card">
                            <img src="${element["urlToImage"]}" class="card-img-top">
                            <div class="card-body">
                            <h5 class="card-title">${element["title"]}</h5>
                            <p class="card-text">${element["content"]}</p>
                            <a href="${element["url"]}" target="_blank" class="btn btn-primary">Selengkapnya</a>
                            </div>
                        </div>
                    </div>`;
            newsHTML += news;
        });
        return newsList.innerHTML = newsHTML
      } 
      
      function message(_msg) {
        return _msg;
      }

  $(document).ready(function(){
    $.ajaxSetup({ cache: false });
    $('#newsSearch').keyup(function(){
        var searchField = $('#newsSearch').val();

        if (searchField != '') {
            newsList.innerHTML = message('<div class="spinner-border text-danger mx-auto mt-5" role="status"><span class="visually-hidden">Loading...</span></div>');
            const berita = fetch('https://newsapi.org/v2/everything?q='+searchField+'&apiKey=fec030d5fa9042b4a90c940be727e9a0');
            berita
            .then(function(res){ return res.json()})
            .then(res => {
              newsList.innerHTML = render(res);
            })
            .catch(err => {
                newsList.innerHTML = message(err.message)
            })
            .finally(() => {
              
            });
        } else {
            newsList.innerHTML = message('<div class="spinner-border text-danger mx-auto mt-5" role="status"><span class="visually-hidden">Loading...</span></div>');
            const berita = fetch('https://newsapi.org/v2/top-headlines?country=id&apiKey=fec030d5fa9042b4a90c940be727e9a0');
            berita
            .then(function(res){ return res.json()})
            .then(res => {
              newsList.innerHTML = render(res);
            })
            .catch(err => {
                newsList.innerHTML = message(err.message)
            })
            .finally(() => {
              
            });
        }
    });
  });